package at.itkolleg;

import dataaccess.MySqlCourseRepository;
import dataaccess.MySqlStudentRepository;
import dataaccess.MysqlDatabaseConnection;
import ui.Cli;

import java.sql.Connection;
import java.sql.SQLException;

public class Main {
    public static void main(String[] args) {
        Cli cli = null;
        try {
            cli = new Cli(new MySqlCourseRepository(),new MySqlStudentRepository());
        } catch (SQLException e) {
            System.out.println("Datenbankfehler" + e.getMessage() + "SQL Sate" + e.getSQLState());
        } catch (ClassNotFoundException e) {
            System.out.println("Datenbankfehler" + e.getMessage());
        }
        cli.start();
    }
}
