package ui;

import dataaccess.DatabaseException;
import dataaccess.MyCourseRepository;
import dataaccess.MyStudentRepository;
import domain.Course;
import domain.CourseType;
import domain.InvalidValueException;
import domain.Student;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class Cli {

    Scanner scanner;
    MyCourseRepository repoCourse;
    MyStudentRepository repoStudent;

    public Cli(MyCourseRepository repoCourse, MyStudentRepository repoStudent) {
        this.scanner = new Scanner(System.in);
        this.repoCourse = repoCourse;
        this.repoStudent = repoStudent;
    }

    public void start() {
        String input = "-";
        while (!input.equals("x")) {
            showMeneu();
            input = scanner.nextLine();
            switch (input) {
                case "1":
                    addCourse();
                    break;
                case "2":
                    showAllCourses();
                    break;
                case "3":
                    showCourseDetails();
                    break;
                case "x":
                    System.out.println("Auf Wiedersehen");
                    break;
                case "4":
                    updateCourseDetails();
                    break;
                case "5":
                    deleteCourse();
                    break;
                case "6":
                    searchCourseWithDescriptionOrName();
                    break;
                case "7":
                    runningCourses();
                    break;
                case "8":
                    addStudent();
                    break;
                case "9":
                    showAllStudents();
                    break;
                case "10":
                    updateStudent();
                    break;
                case "11":
                    deleteStudent();
                    break;
                case "12":
                    searchStudentByName();
                    break;
                case "13":
                    searchStudentByBirthdate();
                    break;
                case "14":
                    searchStudentByBirthdateBetween();
                    break;
                default:
                    inputError();
                    break;
            }
        }
        scanner.close();
    }

    private void searchStudentByBirthdateBetween() {
        try {
            List<Student> studentList = new ArrayList<>();
            System.out.println("Bitte Startdatum eingeben (YYYY-MM-DD)");
            Date startDate = Date.valueOf(scanner.nextLine());
            System.out.println("Bitte Enddatum eingeben (YYYY-MM-DD)");
            Date endDate = Date.valueOf(scanner.nextLine());

            studentList = repoStudent.findStudentsBetweenBirthdate(startDate,endDate);
            for (Student student : studentList) {
                System.out.println(student);
            }
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler bei Suche nach Datum: " + databaseException.getMessage());

        } catch (Exception ex) {
            System.out.println("Unbekannter Fehler bei Suche nach Datum " + ex.getMessage());
        }
    }

    private void searchStudentByBirthdate() {
        try {
            List<Student> studentList = new ArrayList<>();
            System.out.println("Nach welchem Datum soll gesucht werden? (YYYY-MM-DD)");
            Date birthdate = Date.valueOf(scanner.nextLine());
            studentList = repoStudent.findStudentsByBirthdate(birthdate);
            for (Student student : studentList) {
                System.out.println(student);
            }
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler bei Suche nach Datum: " + databaseException.getMessage());

        } catch (Exception ex) {
            System.out.println("Unbekannter Fehler bei Suche nach Datum " + ex.getMessage());
        }
    }

    private void searchStudentByName() {
        try {
            List<Student> studentList = new ArrayList<>();
            System.out.println("Nach welchem Namen soll gesucht werden?");
            String name = scanner.nextLine();
            studentList = repoStudent.findStudentsByName(name);
            for (Student student : studentList) {
                System.out.println(student);
            }
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler bei Suche nach Namen: " + databaseException.getMessage());

        } catch (Exception ex) {
            System.out.println("Unbekannter Fehler bei Suche nach namen " + ex.getMessage());
        }
    }

    private void deleteStudent() {
        try {
            System.out.println("Welcher Student soll gelöscht werden? Bitte die id angeben");
            long id = Long.parseLong(scanner.nextLine());
            repoStudent.deleteById(id);

        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim löschen " + databaseException.getMessage());
        } catch (Exception e) {
            System.out.println("Unbekannter Fehler beim löschen " + e.getMessage());
        }
    }

    private void updateStudent() {
        System.out.println("Für welche ID möchten Sie den Studenten ändern?");
        Long studentId = Long.parseLong(scanner.nextLine());

        try {
            Optional<Student> studentOptional = repoStudent.getById(studentId);
            if (studentOptional.isEmpty()) {
                System.out.println("Student mit der gegebenen ID nicht in der DB");
            } else {
                Student student = studentOptional.get();
                System.out.println("Änderungen für folgenden Student ");
                System.out.println(student);

                String firstname, lastname, birthdate;
                System.out.println("Bitte neue Studentendaten eingeben (Enter falls keine Änderung gewünscht ist)");
                System.out.println("Vorname:");
                firstname = scanner.nextLine();
                System.out.println("Nachname:");
                lastname = scanner.nextLine();
                System.out.println("Geburtsdatum (YYYY-MM-DD):");
                birthdate = scanner.nextLine();

                Optional<Student> optionalStudentUpdated = repoStudent.update(
                        new Student(student.getID(),
                                firstname.equals("") ? student.getFirstname() : firstname,
                                lastname.equals("") ? student.getLastname() : lastname,
                                birthdate.equals("") ? student.getBirthdate() : Date.valueOf(birthdate))
                );

                optionalStudentUpdated.ifPresentOrElse(
                        (c) -> System.out.println("Student aktualisiert" + c), //hier der if Zweig
                        () -> System.out.println("Student konnte nicht aktualisiert werden!") // hier des else Zweig
                );
            }
        } catch (IllegalArgumentException illegalArgumentException) {
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        } catch (InvalidValueException invalidValueException) {
            System.out.println("Studentendaten nicht korrekt " + invalidValueException.getMessage());
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim ändern " + databaseException.getMessage());
        } catch (Exception ex) {
            System.out.println("Unbekannter Fehler beim ändern " + ex.getMessage());
        }
    }

    private void addStudent() throws InvalidValueException {
        try {
            String firstname, lastname;
            Date birthdate;
            System.out.println("Neuen Studenten hinzufügen");
            System.out.println("Bitte Vorname eingeben");
            firstname = scanner.nextLine();
            if (firstname.equals("")) throw new InvalidValueException("Vorname darf nicht leer sein");

            System.out.println("Bitte Nachname eingeben");
            lastname = scanner.nextLine();
            if (lastname.equals("")) throw new InvalidValueException("Nachname darf nicht leer sein");

            System.out.println("Bitte Geburtsdatum eingeben (YYYY-MM-DD)");
            birthdate = Date.valueOf(scanner.nextLine());
            if (birthdate == null) throw new InvalidValueException("Geburtsdatum darf nicht leer sein");

            Optional<Student> optionalStudent = repoStudent.insert(new Student(firstname, lastname, birthdate));
            if (optionalStudent.isPresent()) {
                System.out.println("Student angelegt " + optionalStudent.get());
            } else {
                System.out.println("Student konnte nicht angelegt werden");
            }

        } catch (IllegalArgumentException illegalArgumentException) {
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        } catch (InvalidValueException invalidValueException) {
            System.out.println("Studentendaten nicht korrekt " + invalidValueException.getMessage());
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim einfügen " + databaseException.getMessage());
        } catch (Exception ex) {
            System.out.println("Unbekannter Fehler beim einfügen " + ex.getMessage());
        }

    }

    private void showAllStudents() {
        try {
            System.out.println("Alle Studenten anzeigen");
            List<Student> studentList = new ArrayList<>();
            studentList = repoStudent.getAll();

            if (studentList.size() > 0) {
                for (Student student : studentList) {
                    System.out.println(student);
                }
            } else {
                System.out.println("Keine Studenten gefunden");
            }
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim Anzeigen aller Studenten " + databaseException.getMessage());
        } catch (Exception ex) {
            System.out.println("Unbekannter Fehler beim Anzeigen aller Studenten " + ex.getMessage());
        }
    }

    private void runningCourses() {
        System.out.println("Aktuell laufende Kurse: ");
        List<Course> courseList;

        try {
            courseList = repoCourse.findAllRunningCourses();
            for (Course course : courseList) {
                System.out.println(course);
            }

        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler bei Anzeige der laufenden Kurse: " + databaseException.getMessage());

        } catch (Exception ex) {
            System.out.println("Unbekannter Fehler bei Anzeige der laufenden Kurse");
        }
    }

    private void searchCourseWithDescriptionOrName() {
        System.out.println("Geben Sie einen Suchbegriff an");
        String seachString = scanner.nextLine();
        List<Course> courseList;

        try {
            courseList = repoCourse.findAllCoursesByNameOrDescription(seachString);
            for (Course course : courseList) {
                System.out.println(course);
            }
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler bei der Kurssuche " + databaseException.getMessage());
        } catch (Exception ex) {
            System.out.println("Unbekannter Fehler bei der Kurssuche " + ex.getMessage());
        }
    }

    private void deleteCourse() {
        System.out.println("Welchen Kurs möchten Sie löschen? Bitte ID eingeben");
        long courseIdToDelete = Long.parseLong(scanner.nextLine());

        try {
            repoCourse.deleteById(courseIdToDelete);
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim löschen " + databaseException.getMessage());
        } catch (Exception e) {
            System.out.println("Unbekannter Fehler beim löschen " + e.getMessage());
        }
    }

    private void updateCourseDetails() {
        System.out.println("Für welche Kurs-Id möchten Sie die Kursdetails ändern?");
        Long courseId = Long.parseLong(scanner.nextLine());

        try {
            Optional<Course> courseOptional = repoCourse.getById(courseId);
            if (courseOptional.isEmpty()) {
                System.out.println("Kurs mit der gegebenen ID nicht in der DB");
            } else {
                Course course = courseOptional.get();
                System.out.println("Änderungen für folgenden Kurs ");
                System.out.println(course);

                String name, description, hours, dateFrom, dateTo, courseType;
                System.out.println("Bitte neue Kursdaten eingeben (Enter falls keine Änderung gewünscht ist)");
                System.out.println("Name:");
                name = scanner.nextLine();
                System.out.println("Description:");
                description = scanner.nextLine();
                System.out.println("Stundenanzahl:");
                hours = scanner.nextLine();
                System.out.println("Startdatum");
                dateFrom = scanner.nextLine();
                System.out.println("Enddatum");
                dateTo = scanner.nextLine();
                System.out.println("Kurstyp (ZA/BF/FF/OE");
                courseType = scanner.nextLine();

                Optional<Course> optionalCourseUpdated = repoCourse.update(
                        new Course(course.getID(),
                                name.equals("") ? course.getName() : name,
                                description.equals("") ? course.getDescription() : description,
                                hours.equals("") ? course.getHours() : Integer.parseInt(hours),
                                dateFrom.equals("") ? course.getBeginDate() : Date.valueOf(dateFrom),
                                dateFrom.equals("") ? course.getEndDate() : Date.valueOf(dateTo),
                                courseType.equals("") ? course.getCourseType() : CourseType.valueOf(courseType))
                );

                optionalCourseUpdated.ifPresentOrElse(
                        (c) -> System.out.println("Kurs aktualisiert" + c), //hier der if Zweig
                        () -> System.out.println("Kurs konnte nicht aktualisiert werden!") // hier des else Zweig
                );
            }
        } catch (IllegalArgumentException illegalArgumentException) {
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        } catch (InvalidValueException invalidValueException) {
            System.out.println("Kursdaten nicht korrekt " + invalidValueException.getMessage());
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim ändern " + databaseException.getMessage());
        } catch (Exception ex) {
            System.out.println("Unbekannter Fehler beim ändern " + ex.getMessage());
        }

    }

    private void addCourse() {
        String name, description;
        int hours;
        Date beginndate, enddate;
        CourseType courseType;
        try {
            System.out.println("Bitte aller Kursdaten angeben");
            System.out.println("Name: ");
            name = scanner.nextLine();
            if (name.equals("")) throw new IllegalArgumentException("Eingabe darf nicht null sein");

            System.out.println("Beschreibung: ");
            description = scanner.nextLine();
            if (description.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein");

            System.out.println("Anzahl der Stunden");
            hours = Integer.parseInt(scanner.nextLine());

            System.out.println("Startdatum (YYYY-MM-DD");
            beginndate = Date.valueOf(scanner.nextLine());

            System.out.println("Enddatum (YYYY-MM-DD");
            enddate = Date.valueOf(scanner.nextLine());

            System.out.println("Kurstyp (ZA/BF/FF/OE");
            courseType = CourseType.valueOf(scanner.nextLine());

            Optional<Course> optionalCourse = repoCourse.insert(new Course(name, description, hours, beginndate, enddate, courseType));
            if (optionalCourse.isPresent()) {
                System.out.println("Kurs angelegt " + optionalCourse.get());
            } else {
                System.out.println("Kurs konnte nicht angelegt werden");
            }


        } catch (IllegalArgumentException illegalArgumentException) {
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        } catch (InvalidValueException invalidValueException) {
            System.out.println("Kursdaten nicht korrekt " + invalidValueException.getMessage());
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim einfügen " + databaseException.getMessage());
        } catch (Exception ex) {
            System.out.println("Unbekannter Fehler beim einfügen " + ex.getMessage());
        }

    }

    private void showCourseDetails() {
        System.out.println("Für welchen Kurs möchten Sie die Kursdetails anzeigen");
        Long courseId = Long.parseLong(scanner.nextLine());
        try {
            Optional<Course> courseOptional = repoCourse.getById(courseId);
            if (courseOptional.isPresent()) {
                System.out.println(courseOptional.get());
            } else {
                System.out.printf("Kurd mit der ID %d nicht gefunden", courseId);
            }
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler bei Kursdetailanzeige " + databaseException.getMessage());
        } catch (Exception ex) {
            System.out.println("Unbekannter Fehler bei Kursdetailanzeige " + ex.getMessage());
        }
    }

    private void showAllCourses() {
        List<Course> courseListe = null;

        try {
            courseListe = repoCourse.getAll();
            if (courseListe.size() > 0) {
                for (Course course : courseListe) {
                    System.out.println(course);
                }
            } else {
                System.out.println("Kursliste leer");
            }
        } catch (DatabaseException dbException) {
            System.out.println("Datenbankfehler bei Anzeige aller Kurse: " + dbException.getMessage());
        } catch (Exception ex) {
            System.out.println("unbekannter Fehler bei Anzeige aller Kurse " + ex.getMessage());
        }

    }

    private void inputError() {
        System.out.println("Bitte nur die Zahlen der Menüauswahl eingeben!");
    }

    private void showMeneu() {
        System.out.println("---------------Kursmanagement-----------");
        System.out.println("(1) Kurs eingeben \t (2) Alle Kurse anzeigen\t (3) Kursdetails anzeigen \t");
        System.out.println("(4) Kursdetails ändern \t (5) Kurs löschen \t (6) Kurs nach Name und Beschreibung suchen");
        System.out.println("(7) laufende Kurse anzeigen\t (8) Student eingeben\t (9) Alle Studenten ausgeben");
        System.out.println("(10) Student ändern \t (11) Student löschen \t (12) Studenten nach Namen suchen ");
        System.out.println("(13) Student nach Geburtsdatum suchen \t (14) Studenten nach Geburtsdatum zwischen suchen ");
        System.out.println("(x) Ende");
    }
}
