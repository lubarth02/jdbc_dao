package dataaccess;

import domain.Course;
import domain.CourseType;
import domain.Student;
import util.Assert;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MySqlCourseRepository implements MyCourseRepository {

    private Connection conn;

    public MySqlCourseRepository() throws SQLException, ClassNotFoundException {
        this.conn = MysqlDatabaseConnection.getConnection("jdbc:mysql://localhost:3306/kurssystem", "root", "");

    }

    @Override
    public Optional<Course> insert(Course entity) {
        Assert.notNull(entity);

        try {
            String sql = "INSERT INTO `courses` (`name`, `description`, `hours`, `beginndate`, `enddate`, `coursetype`) VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement statement = conn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS); //generierte id wird zurückgeliefert
            statement.setString(1, entity.getName());
            statement.setString(2, entity.getDescription());
            statement.setInt(3, entity.getHours());
            statement.setDate(4, entity.getBeginDate());
            statement.setDate(5, entity.getEndDate());
            statement.setString(6, entity.getCourseType().toString());

            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0) {
                return Optional.empty();
            }

            ResultSet generatedkeys = statement.getGeneratedKeys();
            if (generatedkeys.next()) {
                return this.getById(generatedkeys.getLong(1));
            } else {
                return Optional.empty();
            }
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }

    }

    @Override
    public Optional<Course> getById(Long id) {
        Assert.notNull(id);
        if (countCoursesInDbWithId(id) == 0) {
            return Optional.empty(); //kein Kurs gefunden
        } else {

            try {
                String sql = "SELECT * FROM `courses` WHERE `id` = ?";
                PreparedStatement statement = conn.prepareStatement(sql);
                statement.setLong(1, id);
                ResultSet resultSet = statement.executeQuery();
                List<Course> courseList = buildCourse(resultSet);
                return Optional.of(courseList.get(0)); //Kurs an der Stelle 0 wird zurückgegeben da es nur einen Kurs pro id gibt.

            } catch (SQLException sqlException) {
                throw new DatabaseException(sqlException.getMessage());
            }
        }
    }

    private int countCoursesInDbWithId(Long id) {
        try {
            String countSql = "SELECT COUNT(*) FROM `courses` WHERE `id`=?";
            PreparedStatement statement = conn.prepareStatement(countSql);
            statement.setLong(1, id);
            ResultSet resultSetCount = statement.executeQuery();
            resultSetCount.next();
            int courseCount = resultSetCount.getInt(1);
            return courseCount;

        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }

    }

    @Override
    public List<Course> getAll() {
        String sql = "SELECT * FROM `courses`";
        try {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            return buildCourse(resultSet);

        } catch (SQLException e) {
            throw new DatabaseException("Database error occured!");
        }
    }

    @Override
    public Optional<Course> update(Course entity) {
        Assert.notNull(entity);

        String sql = "UPDATE `courses` SET `name` = ?, `description` = ?, `hours` = ?, `beginndate` = ?, `enddate` = ?, `coursetype` = ? WHERE `courses`.`id` = ?";

        if (countCoursesInDbWithId(entity.getID()) == 0) {
            return Optional.empty();
        } else {
            try {
                PreparedStatement statement = conn.prepareStatement(sql);
                statement.setString(1, entity.getName());
                statement.setString(2, entity.getDescription());
                statement.setInt(3, entity.getHours());
                statement.setDate(4, entity.getBeginDate());
                statement.setDate(5, entity.getEndDate());
                statement.setString(6, entity.getCourseType().toString());
                statement.setLong(7, entity.getID());

                int affectedRows = statement.executeUpdate();
                if (affectedRows == 0) {
                    return Optional.empty();
                } else {
                    return this.getById(entity.getID());
                }

            } catch (SQLException sqlException) {
                throw new DatabaseException(sqlException.getMessage());
            }
        }
    }

    @Override
    public void deleteById(Long id) {
        Assert.notNull(id);
        String sql = "DELETE FROM `courses` WHERE `id` = ?";
        if (countCoursesInDbWithId(id) == 1) {
            PreparedStatement statement = null;
            try {
                statement = conn.prepareStatement(sql);
                statement.setLong(1, id);
                statement.executeUpdate();
            } catch (SQLException sqlException) {
                throw new DatabaseException(sqlException.getMessage());
            }

        }
    }

    @Override
    public List<Course> findAllCoursesByName(String name) {
        return null;
    }

    @Override
    public List<Course> findAllCoursesByDescription(String description) {
        return null;
    }

    @Override
    public List<Course> findAllCoursesByNameOrDescription(String searchText) {

        try {
            String sql = "SELECT * FROM `courses` WHERE LOWER(`description`) LIKE LOWER(?)" +
                    "OR LOWER(`name`) LIKE LOWER(?) ";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, "%" + searchText + "%");
            statement.setString(2, "%" + searchText + "%");
            ResultSet resultSet = statement.executeQuery();
            return buildCourse(resultSet);

        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Course> findAllCoursesByStartDate(Date endDate) {
        return null;
    }

    @Override
    public List<Course> findAllCoursesByCourseType(CourseType courseType) {
        return null;
    }

    @Override
    public List<Course> findAllRunningCourses() {

        String sql = "SELECT * FROM `courses` WHERE NOW()<`enddate`";

        try {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            return buildCourse(resultSet);
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    private List<Course> buildCourse(ResultSet resultSet) {
        try {
            List<Course> courseList = new ArrayList<>();
            while (resultSet.next()) {

                courseList.add(new Course(
                        resultSet.getLong("id"),
                        resultSet.getString("name"),
                        resultSet.getString("description"),
                        resultSet.getInt("hours"),
                        resultSet.getDate("beginndate"),
                        resultSet.getDate("enddate"),
                        CourseType.valueOf(resultSet.getString("coursetype")))
                );
            }
            return courseList;
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }

    }
}
