package dataaccess;

import domain.Course;
import domain.Student;
import util.Assert;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MySqlStudentRepository implements MyStudentRepository {
    private Connection conn;

    public MySqlStudentRepository() throws SQLException, ClassNotFoundException {
        this.conn = MysqlDatabaseConnection.getConnection("jdbc:mysql://localhost:3306/kurssystem", "root", "");
    }

    @Override
    public Optional<Student> insert(Student entity) {
        Assert.notNull(entity);
        String sql = "INSERT INTO `students` (`firstname`, `lastname`, `birthdate`) VALUES (?, ?, ?)";

        try {
            PreparedStatement statement = conn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            statement.setString(1, entity.getFirstname());
            statement.setString(2, entity.getLastname());
            statement.setDate(3, entity.getBirthdate());
            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0) {
                return Optional.empty();
            }

            ResultSet generatedkeys = statement.getGeneratedKeys();
            if (generatedkeys.next()) {
                return this.getById(generatedkeys.getLong(1));
            } else {
                return Optional.empty();
            }
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    private int countStudentsInDbWithId(Long id) {
        try {
            String countSql = "SELECT COUNT(*) FROM `students` WHERE `id`=?";
            PreparedStatement statement = conn.prepareStatement(countSql);
            statement.setLong(1, id);
            ResultSet resultSetCount = statement.executeQuery();
            resultSetCount.next();
            int studentCount = resultSetCount.getInt(1);
            return studentCount;

        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }

    }

    @Override
    public Optional<Student> getById(Long id) {
        Assert.notNull(id);

        if (countStudentsInDbWithId(id) == 0) {
            return Optional.empty();
        } else {
            String sql = "SELECT * FROM `students` WHERE id=?";

            try {
                PreparedStatement statement = conn.prepareStatement(sql);
                statement.setLong(1, id);
                ResultSet resultSet = statement.executeQuery();
                List<Student> studentList = buildStudent(resultSet);
                return Optional.of(studentList.get(0)); //Student an Position 0 wird zurückgegeben da nur 1 Student pro id vorhanden ist.

            } catch (SQLException sqlException) {
                throw new DatabaseException(sqlException.getMessage());
            }
        }
    }

    @Override
    public List<Student> getAll() {
        try {
            String sql = "SELECT * FROM `students`";
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            return buildStudent(resultSet);
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public Optional<Student> update(Student entity) {

        try {
            String sql = "UPDATE `students` SET `firstname` = ?, `lastname` = ?, `birthdate` = ? WHERE `students`.`id` = ?";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, entity.getFirstname());
            statement.setString(2, entity.getLastname());
            statement.setDate(3, entity.getBirthdate());
            statement.setLong(4, entity.getID());

            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                return Optional.empty();
            } else {
                return this.getById(entity.getID());
            }
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public void deleteById(Long id) {

        try {
            String sql = "DELETE FROM `students` WHERE `students`.`id` = ?";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }

    }

    @Override
    public List<Student> findStudentsByName(String name) {
        try {
            String sql = "SELECT * FROM `students` WHERE LOWER(`firstname`) LIKE LOWER(?)" +
                    "OR LOWER(`lastname`) LIKE LOWER(?) ";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, "%" + name + "%");
            statement.setString(2, "%" + name + "%");

            ResultSet resultSet = statement.executeQuery();
            return buildStudent(resultSet);

        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Student> findStudentsByBirthdate(Date birthdate) {
        try {
            String sql = "SELECT * FROM `students` WHERE `birthdate` = ?  ";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setDate(1, birthdate);
            ResultSet resultSet = statement.executeQuery();
            return buildStudent(resultSet);
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Student> findStudentsBetweenBirthdate(Date beginnDate, Date endDate) {
        try {
            List<Student> studentList = new ArrayList<>();
            String sql = "SELECT * FROM `students` WHERE `birthdate` BETWEEN ? AND ?";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setDate(1, beginnDate);
            statement.setDate(2, endDate);

            ResultSet resultSet = statement.executeQuery();
            return buildStudent(resultSet);
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    private List<Student> buildStudent(ResultSet resultSet) {
        try {
            List<Student> studentList = new ArrayList<>();
            while (resultSet.next()) {

                studentList.add(new Student(
                        resultSet.getLong("id"),
                        resultSet.getString("firstname"),
                        resultSet.getString("lastname"),
                        resultSet.getDate("birthdate")
                ));
            }
            return studentList;
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }

    }
}
