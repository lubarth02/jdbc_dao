package dataaccess;

import domain.Student;

import java.sql.Date;
import java.util.List;

public interface MyStudentRepository extends BaseRepository<Student,Long> {

    List<Student> findStudentsByName(String name);

    List<Student> findStudentsByBirthdate(Date birthdate);

    List<Student> findStudentsBetweenBirthdate(Date beginnDate, Date endDate);
}
