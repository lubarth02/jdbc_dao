package domain;

import java.sql.Date;

public class Student extends BaseEntity {
    private String firstname;
    private String lastname;
    private Date birthdate;

    public Student(Long id, String firstname, String lastname, Date birthdate) {
        super(id);
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthdate = birthdate;
    }

    public Student(String firstname, String lastname, Date birthdate) {
        super(null);
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthdate = birthdate;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) throws InvalidValueException {
        if (firstname.equals("")){
            throw new InvalidValueException("Vorname darf nicht leer sein");
        }
        else if (firstname.length() > 30){
            throw new InvalidValueException("Vorname darf nicht länger als 30 Zeichen sein");
        }
        else {
            this.firstname = firstname;
        }
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) throws InvalidValueException {
        if (lastname.equals("")){
            throw new InvalidValueException("Nachname darf nicht leer sein");
        }
        else if (lastname.length() > 30){
            throw new InvalidValueException("Nachname darf nicht länger als 30 Zeichen sein");
        }
        else {
            this.lastname = lastname;
        }
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) throws InvalidValueException {
        if (birthdate == null){
            throw new InvalidValueException("Geburtsdatum darf nicht leer sein");
        }
    }

    @Override
    public String toString() {
        return "Student{" +
                "id='" + this.getID() + '\'' +
                "firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", birthdate=" + birthdate +
                '}';
    }
}
