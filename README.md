# JDBC DAO
DAO (Data Acces Objekt) ermöglicht den Zugriff auf verschiedene Datenbanken.
Die verwendete Datenbank kann hierbei ausgetauscht werden ohne ein Quellcode ändern zu müssen.
Dies wird erreicht in dem ein Interface Base Entity mit den Funktionen, die alle Entitäten haben sollen definiert.
Von diesem generellen Interface erbt ein Interface der Domänenklasse wie z.B. Course.
Dieses kann noch weitere spezielle Methoden für die Domänenklasse definieren.
Ein konkretes Repo der Domänenklasse das an eine Datenbanktechnologie gebunden ist 
z.B. MySqlCourseRepo implementiert die generellen und speziellen Methoden für die Domänenklasse.
Die Verbindung zur Datenbank wird ebenso in einer eigenen Klasse hergestellt,
die das MySQlCourseRepo verwendet. Für den Aufbau der Verbindung zur Datenbank
das Singleton Pattern benutzt, dies bedeutet, dass gleichzeitig nur eine Verbindung zur 
Datenbank aufgebaut wird.

## objektrelationales Mapping
Beim objektrelationalen Mapping werden die einzelen Datensätze aus der Datenbank
in Objekte in Java überführt. Dazu werden die von der Datenbank zurückgelieferten
Datensätze in einem Result Set gespeichert. Dieses wird mit einer while Schleife
durchgegangen und für jeden Datensatz wird ein neues Objekt erstellt und dem Konstruktor
die passenden Werte übergeben. Dies sieht folgendermaßen aus:
````java
ResultSet resultSet = statement.executeQuery();
            ArrayList<Course> courseList = new ArrayList<>();
            while (resultSet.next()) {
                courseList.add(new Course(
                        resultSet.getLong("id"),
                        resultSet.getString("name"),
                        resultSet.getString("description"),
                        resultSet.getInt("hours"),
                        resultSet.getDate("beginndate"),
                        resultSet.getDate("enddate"),
                        CourseType.valueOf(resultSet.getString("coursetype")))
                );
            }
````
Das Objekt relationale Mapping ist nur erforderlich, wenn das ausgeführte SQL Statement 
Datensätze zurückliefert. In der Domänenklasse wird mit settern überprüft, dass das
Objekt nur mit sinnvollen Werten initialisier werden kann.

## Exceptions
Mithilfe von Exceptions wird sichergestellt, dass bei auftreten einer Ausnahme nicht
das Programm beendet wird.Bei Ausführung einer Operation auf der Datenbank wird mit einer
SQL Exception sichergestellt, dass der Benutzer bei einem Datenbankfehler informiert wird.
Es können auch mehrere Exceptions behandelt werden. Dies ist sinnvoll um verschiedene
Quellen der Exceptions behandeln zu können.

## CLI
Der Benutzer interagiert mit dem System über eine CLI. In der Klasse Cli werden
über ein Auswahlmenü die Methoden aufgerufen. Die Fälle werden über Switch-Case unterschieden.
Die einzelnen Methoden in der CLI verwenden die Methoden des MySQLCourseRepo,
daher hat die Cli in Datenfelder der Klasse MySQLCourseRepo.

## Buchungen hinzufügen
Zuerst müsste eine Domänenklasse Buchungen erstellt werden, die Datenfelder für die StudentenId und die KursId
sowie für ein Buchungsdatum hat. Diese Domänenklasse würde von der Klasse Entity erben und die Werte der Datenfelder
müssten dem Konstruktor übergeben werden. In den Settern der Datenfelder müsste die korrekte Einhaltung der Businesslogik sichergestellt werden.
In dem Interface MyBuchungRepository würden zusätzlich zu den CRUD Methoden benötigte Methoden definiert werden.
Dies wären z.B. alle Buchungen an einem Datum anzeigen, alle Buchungen zwischen Start und Enddatum anzeigen,
alle laufenden Buchungen anzeigen, alle Buchungen für einen Student anzeigen, alle Buchungen für eine Kurs anzeigen.
In der Klasse MySqlBuchungenRepository würden die Basis CRUD Methoden und die zusätzlichen Methoden ausimplementiert.